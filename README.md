# Project 4: Brevet time calculator with Ajax
coder: Colton Trebbien
email: ctrebbie@uoregon.edu

Credits to Michal Young for the initial version of this code.

## ACP controle times

Kept this in for people who want to know where this data is coming from

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

* Added funtionality if you change the brevit time/date/distance it auto updates all the times  chang   
* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

## error checking Handling

if the user puts in a non number, it wont calculate anything
if the user puts in a negative number a note/warning will pop up
if the user puts in a number larger that 20% of the brevit then a note will pop up saying its to big
 

## Testing

Testing can be run from the docker using nosetests from a running container by running docker exec -it <contianer_id> nosetests


